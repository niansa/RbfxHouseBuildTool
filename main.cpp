#include <Urho3D/Engine/Application.h>
#include <Urho3D/Engine/EngineDefs.h>
#include <Urho3D/Scene/Scene.h>
#include <Urho3D/Scene/Node.h>
#include <Urho3D/Scene/LogicComponent.h>
#include <Urho3D/Scene/Component.h>
#include <Urho3D/Physics/CollisionShape.h>
#include <Urho3D/Physics/RigidBody.h>
#include <Urho3D/Physics/PhysicsWorld.h>
#include <Urho3D/Graphics/Model.h>
#include <Urho3D/Graphics/StaticModel.h>
#include <Urho3D/Graphics/StaticModelGroup.h>
#include <Urho3D/Graphics/Camera.h>
#include <Urho3D/Graphics/Octree.h>
#include <Urho3D/Graphics/Renderer.h>
#include <Urho3D/Graphics/Viewport.h>
#include <Urho3D/Graphics/Light.h>
#include <Urho3D/Graphics/Material.h>
#include <Urho3D/UI/UI.h>
#include <Urho3D/SystemUI/SystemUI.h>
#include <Urho3D/Input/Input.h>
#include <Urho3D/Resource/ResourceCache.h>
#include <nativefiledialog/src/include/nativefiledialog/nfd.h>



using namespace Urho3D;

static constexpr uint8_t NORTH = 0b000001,
                         EAST = 0b000010,
                         SOUTH = 0b000100,
                         WEST = 0b001000,
                         TOP = 0b010000,
                         BOTTOM = 0b100000;


inline
float roundMultiple(float value, float multiple) {
    return static_cast<float>(std::round(static_cast<double>(value)/static_cast<double>(multiple))*static_cast<double>(multiple));
}

static
void scenePrep(Scene *scene);
static
void sceneDeprep(Scene *scene);


class Factory : public LogicComponent {
    URHO3D_OBJECT(Factory, LogicComponent);

    Node *walls, *floors;
    SharedPtr<Material> mat;

public:
    struct WallSource {
        Vector2 from, to;
        int floor = 0;
        float thickness = 0.25f;
        uint8_t sides = NORTH | EAST | SOUTH | WEST;

        float getLength() const {
            return (from - to).Length();
        }
    };

    struct Wall {
        WallSource source;
        Node *north, *east, *south, *west, *root = nullptr;

        void remove() {
            root->Remove();
            root = nullptr;
        }
        bool hasValue() const {
            return root;
        }
        operator bool() const {
            return root;
        }
    };

    struct FloorSource {
        Vector2 from, to;
        int floor = 0;
        uint8_t sides = TOP | BOTTOM;

        float getWidth() const {
            return Abs(from.x_ - to.x_);
        }
        float getHeight() const {
            return Abs(from.y_ - to.y_);
        }
    };

    struct Floor {
        FloorSource source;
        Node *top, *bottom, *root = nullptr;

        void remove() {
            root->Remove();
            root = nullptr;
        }
        bool hasValue() const {
            return root;
        }
        operator bool() const {
            return root;
        }
    };

    float floorHeight = 2.3f;

    using LogicComponent::LogicComponent;

    void Start() override {
        // Create initial roots
        walls = GetScene()->FindChild("Walls");
        if (!walls) {
            walls = GetScene()->CreateChild("Walls");
        }
        floors = GetScene()->FindChild("Floors");
        if (!floors) {
            floors = GetScene()->CreateChild("Floors");
        }
        // Create material
        mat = new Material(context_);
        mat->SetShaderParameter("MatSpecColor", Color(0.0f, 0.8f, 0.8f, 0.0f));
    }

    Wall makeWall(WallSource sWall) {
        Wall fres;

        // Make sure from/to are good
        if (sWall.from.x_ < sWall.to.x_) {
            eastl::swap(sWall.from, sWall.to);
        }

        // Pre calculate stuff
        auto fullLength = sWall.getLength();
        auto halfLength = fullLength * 0.5f;
        auto fullFloorHeight = floorHeight * (sWall.floor + 1);
        auto halfFloorHeight = fullFloorHeight - (floorHeight * 0.5f);

        // Create root node
        fres.root = walls->CreateChild();
        fres.root->AddTag("WallRoot");
        auto mesh = fres.root->CreateChild("Mesh");

        // Create north side
        if (sWall.sides & NORTH) {
            fres.north = mesh->CreateChild("North");
            fres.north->SetPosition(Vector3(0.5f, 0.0f, 0.0f));
            fres.north->SetRotation(Quaternion(Vector3(0.0f, 0.0f, -90.0f)));
        } else {
            fres.north = nullptr;
        }

        // Create south side
        if (sWall.sides & SOUTH) {
            fres.south = mesh->CreateChild("South");
            fres.south->SetPosition(Vector3(-0.5f, 0.0f, 0.0f));
            fres.south->SetRotation(Quaternion(Vector3(0.0f, 0.0f, 90.0f)));
        } else {
            fres.south = nullptr;
        }

        // Create east side
        if (sWall.sides & EAST) {
            fres.east = mesh->CreateChild("East");
            fres.east->SetPosition(Vector3(0.0f, 0.0f, 0.5f));
            fres.east->SetRotation(Quaternion(Vector3(90.0f, 0.0f, 0.0f)));
        } else {
            fres.east = nullptr;
        }

        // Create west side
        if (sWall.sides & WEST) {
            fres.west = mesh->CreateChild("West");
            fres.west->SetPosition(Vector3(0.0f, 0.0f, -0.5f));
            fres.west->SetRotation(Quaternion(Vector3(-90.0f, 0.0f, 0.0f)));
        } else {
            fres.west = nullptr;
        }

        // Create planes
        auto plane = GetSubsystem<ResourceCache>()->GetResource<Model>("Models/Plane.mdl");
        for (auto node : {fres.north, fres.east, fres.south, fres.west}) {
            if (node) {
                node->AddTag("WallSide");
                auto model = node->CreateComponent<StaticModel>();
                model->SetModel(plane);
                model->SetMaterial(mat);
                model->SetCastShadows(true);
            }
        }

        // Set correct scale
        mesh->SetScale(Vector3(fullLength + sWall.thickness, floorHeight, sWall.thickness));

        // Set correct rotation and position
        fres.root->SetWorldRotation(Quaternion(Vector3(0.0f, ToDegrees(acos((sWall.from-sWall.to).Normalized().y_))+90.0f, 0.0f)));
        auto pos = (sWall.from + sWall.to) * 0.5f;
        fres.root->SetWorldPosition(Vector3(pos.x_, halfFloorHeight, pos.y_));

        // Create physics body
        fres.root->CreateComponent<RigidBody>();
        auto collisionShape = fres.root->CreateComponent<CollisionShape>();
        collisionShape->SetSize(mesh->GetScale());

        // Return final wall
        fres.source = sWall;
        return fres;
    }

    Floor makeFloor(FloorSource sFloor) {
        Floor fres;

        // Create root node
        fres.root = floors->CreateChild();
        fres.root->AddTag("FloorRoot");
        auto mesh = fres.root->CreateChild("Mesh");

        // Create top side
        if (sFloor.sides & TOP) {
            fres.top = mesh->CreateChild("Top");
        } else {
            fres.top = nullptr;
        }

        // Create bottom side
        if (sFloor.sides & BOTTOM) {
            fres.bottom = mesh->CreateChild("Bottom");
            fres.bottom->SetRotation(Quaternion(Vector3(0.0f, 0.0f, 180.0f)));
        } else {
            fres.bottom = nullptr;
        }

        // Create planes
        auto plane = GetSubsystem<ResourceCache>()->GetResource<Model>("Models/Plane.mdl");
        for (auto node : {fres.top, fres.bottom}) {
            if (node) {
                node->AddTag("FloorSide");
                auto model = node->CreateComponent<StaticModel>();
                model->SetModel(plane);
                model->SetMaterial(mat);
                model->SetCastShadows(true);
            }
        }

        // Set correct scale
        mesh->SetScale(Vector3(sFloor.getWidth(), 0.01f, sFloor.getHeight()));

        // Set correct rotation and position
        auto pos = (sFloor.from + sFloor.to) * 0.5f;
        fres.root->SetWorldPosition(Vector3(pos.x_, floorHeight * sFloor.floor, pos.y_));

        // Create physics body
        fres.root->CreateComponent<RigidBody>();
        auto collisionShape = fres.root->CreateComponent<CollisionShape>();
        collisionShape->SetSize(mesh->GetScale());

        // Return final floor
        fres.source = sFloor;
        return fres;
    }
};

class Checkerboard : public LogicComponent {
    URHO3D_OBJECT(Checkerboard, LogicComponent)

    Node *root = nullptr;
    SharedPtr<Material> mat;

public:
    float scale;
    int floor;

    using LogicComponent::LogicComponent;

    void Start() override {
        // Create material
        mat = new Material(context_);
        mat->SetShaderParameter("MatDiffColor", Color(0.5f, 0.0f, 0.25f, 0.0f));
        // Set values to default
        resetScale();
        resetFloor();
        // Make initial checkerboard
        make();
    }

    void Update(float) override {
        bool needsUpdate = false;
        // Detect keypresses
        auto input = GetSubsystem<Input>();
        if (input->GetKeyDown(Key::KEY_SHIFT)) {
            if (input->GetKeyPress(Key::KEY_UP)) {
                scale += 0.1f;
                needsUpdate = true;
            }
            if (input->GetKeyPress(Key::KEY_DOWN)) {
                scale -= 0.1f;
                needsUpdate = true;
            }
            if (input->GetKeyPress(Key::KEY_HASH)) {
                resetScale();
                needsUpdate = true;
            }
        } else {
            if (input->GetKeyPress(Key::KEY_UP)) {
                floor += 1;
                needsUpdate = true;
            }
            if (input->GetKeyPress(Key::KEY_DOWN)) {
                floor -= 1;
                needsUpdate = true;
            }
            if (input->GetKeyPress(Key::KEY_HASH)) {
                resetFloor();
                needsUpdate = true;
            }
        }
        // Update if needed
        if (needsUpdate) {
            make();
        }
    }

    void make() {
        // Get factory
        auto factory = GetScene()->GetComponent<Factory>();
        // Prepare root node
        if (!root) {
            root = GetNode()->CreateChild("Floor")->CreateChild("Mesh");
        } else {
            root->RemoveAllChildren();
        }
        // Prepare model group
        auto mGroup = root->CreateComponent<StaticModelGroup>();
        // Make actual pattern
        auto plane = GetSubsystem<ResourceCache>()->GetResource<Model>("Models/Plane.mdl");
        constexpr unsigned height = 99,
                           width = 100;
        root->SetPosition(Vector3(-(width*scale*0.5f), floor*factory->floorHeight/*-scale*0.5f*/+0.001f, -(height*scale*0.5f)));
        bool o = true;
        for (unsigned x = 0; x != width; x++) {
            for (unsigned y = 0; y != height; y++) {
                if (o) {
                    auto tile = root->CreateChild();
                    tile->AddTag("CheckerboardTile");
                    tile->SetScale(scale);
                    tile->SetPosition(Vector3(x*scale, 0.0f, y*scale));
                    auto model = tile->CreateComponent<StaticModel>();
                    model->SetModel(plane);
                    model->SetMaterial(mat);
                    mGroup->AddInstanceNode(tile);
                }
                o = !o;
            }
        }
    }

    void destroy() {
        auto p = root->GetParent();
        root->Remove();
        root = nullptr;
        if (p->GetChildren().empty()) {
            p->Remove();
        }
    }

    void resetScale() {
        scale = 1.0f;
    }
    void resetFloor() {
        floor = 0;
    }
    Node *getRoot() const {
        return root;
    }
};


class MainController final : public LogicComponent {
    URHO3D_OBJECT(MainController, LogicComponent);

    Camera *camera;
    SharedPtr<Node> lastLookingAt = nullptr;
    Vector2 from, to;
    Factory::Wall wallPreview;
    Factory::Floor floorPreview;
    bool inBuild = false;
    SharedPtr<Checkerboard> cb;
    float wallThickness = 0.25f;
    eastl::string filename;

    enum class BuildMode {
        wall, floor
    } buildMode = BuildMode::wall;

public:
    using LogicComponent::LogicComponent;

    void Start() override {
        camera = GetNode()->GetOrCreateComponent<Camera>();
        cb = GetScene()->GetComponent<Checkerboard>();

        // Set viewport camera
        auto* renderer = GetSubsystem<Renderer>();
        SharedPtr<Viewport> viewport(new Viewport(context_, GetScene(), camera));
        renderer->SetViewport(0, eastl::move(viewport));
    }

    void FixedUpdate(float) override {
        // Detect keys and maintain movement vector
        auto input = GetSubsystem<Input>();
        Vector3 movement = Vector3::ZERO;
        float speed = 0.05f;
        if (input->GetKeyDown(Key::KEY_SHIFT)) {
            speed *= 2.0f;
        }
        if (input->GetKeyDown(Key::KEY_W)) {
            movement += Vector3::FORWARD;
        }
        if (input->GetKeyDown(Key::KEY_S)) {
            movement -= Vector3::FORWARD;
        }
        if (input->GetKeyDown(Key::KEY_A)) {
            movement += Vector3::LEFT;
        }
        if (input->GetKeyDown(Key::KEY_D)) {
            movement -= Vector3::LEFT;
        }
        if (input->GetKeyDown(Key::KEY_E)) {
            movement += Vector3::UP;
        }
        if (input->GetKeyDown(Key::KEY_Q)) {
            movement -= Vector3::UP;
        }
        if (movement.LengthSquared() > 0.0f) {
            movement.Normalize();
        }
        // Calculate new position
        auto nPos = GetNode()->GetPosition()+GetNode()->GetWorldRotation()*movement*speed;
        nPos.y_ = Max(1.1f, nPos.y_);
        GetNode()->SetPosition(nPos);
        // Get looked at Node
        auto *graphics = GetSubsystem<Graphics>();
        IntVector2 pos = GetSubsystem<UI>()->GetCursorPosition();
        auto ray = camera->GetScreenRay(float(pos.x_) / graphics->GetWidth(), float(pos.y_) / graphics->GetHeight());
        ea::vector<RayQueryResult> results;
        RayOctreeQuery query(results, ray, RAY_TRIANGLE, 100.0f, DRAWABLE_GEOMETRY);
        GetScene()->GetComponent<Octree>()->RaycastSingle(query);
        // Checked if there is any looked at node
        while (!results.empty()) {
            auto lookingAt = results[0].node_;
            // Make sure node is not part of a preview
            if (lookingAt->GetParent() && lookingAt->GetParent()->GetParent() && lookingAt->GetParent()->GetParent()->HasTag("Preview")) {
                results.pop_front();
                continue;
            }
            // Check if a new object is being looked at
            if (lookingAt != lastLookingAt) {
                // Update highlight
                highlight(lookingAt);
                if (lastLookingAt) {
                    unhighlight(lastLookingAt);
                }
                lastLookingAt = lookingAt;
                // Update wall preview
                updatePreview();
            }
            break;
        }
    }

    void Update(float) override {
        auto input = GetSubsystem<Input>();
        // Detect mouse movement if grabbed
        if (input->IsMouseGrabbed()) {
            auto mMove = input->GetMouseMove();
            if (mMove.x_ || mMove.y_) {
                auto rot = GetNode()->GetRotation().EulerAngles();
                rot.x_ = Max(Min(rot.x_ + mMove.y_ / 8.0f, 80), -80);
                rot.y_ += mMove.x_ / 4.0f;
                GetNode()->SetRotation(Quaternion(rot));
            }
        }
        // Handle wall building
        if (lastLookingAt) {
            // Wall creation
            if (input->GetMouseButtonPress(MouseButtonFlags::Enum::MOUSEB_LEFT)) {
                if (inBuild) {
                    auto to3D = lastLookingAt->GetWorldPosition();
                    to = Vector2(to3D.x_, to3D.z_);
                    makeObject(from, to);
                    inBuild = false;
                    stopPreview();
                } else {
                    auto from3D = lastLookingAt->GetWorldPosition();
                    from = Vector2(from3D.x_, from3D.z_);
                    inBuild = true;
                    startPreview();
                }
            }
            // Wall destruction
            if (input->GetMouseButtonPress(MouseButtonFlags::Enum::MOUSEB_RIGHT)) {
                if (inBuild) {
                    inBuild = false;
                    stopPreview();
                } else if (lastLookingAt->HasTag(buildMode == BuildMode::wall ? "WallSide" : "FloorSide")) {
                    if (!input->GetKeyDown(Key::KEY_SHIFT)) {
                        lastLookingAt = lastLookingAt->GetParent()->GetParent();
                    }
                    lastLookingAt->Remove();
                    lastLookingAt = nullptr;
                }
            }
        }
        // Property shortcuts
        if (input->GetKeyPress(Key::KEY_LEFT)) {
            wallThickness -= 0.1f;
            updatePreview();
        }
        if (input->GetKeyPress(Key::KEY_RIGHT)) {
            wallThickness += 0.1f;
            updatePreview();
        }
        if (input->GetKeyPress(Key::KEY_B)) {
            switch (buildMode) {
            case BuildMode::floor: buildMode = BuildMode::wall; break;
            case BuildMode::wall: buildMode = BuildMode::floor; break;
            }
        }
        // Grab/ungrab mouse cursor
        if (input->GetKeyPress(Key::KEY_ESCAPE)) {
            auto currentlyVisible = input->IsMouseVisible();
            input->SetMouseVisible(!currentlyVisible);
            input->SetMouseGrabbed(currentlyVisible);
        }
        // Update UI
        updateUI();
    }

    void updateUI() {
        bool returnAsap = false;
        ui::SetNextWindowSize({600, 800});
        ui::Begin("RBFX Wall building tool", nullptr, ImGuiWindowFlags_MenuBar);
        {
            // Menu
            if (ui::BeginMenuBar()) {
                if (ui::BeginMenu("File")) {
                    if (ui::MenuItem("New", "Ctrl+N")) {
                        GetScene()->FindChild("Walls")->RemoveAllChildren();
                    }
                    if (ui::MenuItem("Open...", "Ctrl+O")) {
                        returnAsap = open();
                    }
                    if (ui::MenuItem("Save", "Ctrl+S")) {
                        returnAsap = save();
                    }
                    if (ui::MenuItem("Save As...", "Ctrl+Shift+S")) {
                        returnAsap = save(true);
                    }
                    if (ui::MenuItem("Add resource dir...", "Ctrl+Shift+O")) {
                        addResourceDir();
                    }
                    if (ui::MenuItem("Close", "Ctrl+X")) {
                        ErrorExit("Exit requested by user", EXIT_SUCCESS);
                    }
                    ui::EndMenu();
                }
                ui::EndMenuBar();
            }
            // Content
            if (!returnAsap) {
                // Overview
                ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "Overview");
                ui::Text("Current floor: %i", cb->floor);
                ui::Text("Current meters per tile: %f", cb->scale);
                ui::Separator();
                // Building properties
                ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "Building properties");
                bool floorMode = buildMode == BuildMode::floor;
                ui::Checkbox("Floor mode", &floorMode);
                buildMode = floorMode ? BuildMode::floor : BuildMode::wall;
                // Checkerboard properties
                ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "Checkerboard properties");
                ui::InputInt("Floor", &cb->floor);
                ui::InputFloat("Scale", &cb->scale);
                if (ui::Button("Apply")) {
                    cb->make();
                }
                ui::Separator();
                // Wall properties
                if (buildMode == BuildMode::wall) {
                    ImGui::TextColored(ImVec4(1.0f, 1.0f, 0.0f, 1.0f), "Wall properties");
                    ui::InputFloat("Thickness", &wallThickness);
                }
            }
        }
        ui::End();
    }

    bool open() noexcept {
        // Get target filename
        nfdchar_t *selectedFile = nullptr;
        if (NFD_OpenDialog("xml", "", &selectedFile, nullptr) == NFD_OKAY) {
            filename = selectedFile;
            NFD_FreePath(selectedFile);
        } else {
            return false;
        }
        // Open that file
        auto scene = GetScene();
        File ser(context_, filename, FileMode::FILE_READ);
        scene->LoadXML(ser);
        scenePrep(scene);
        return true;
    }

    bool save(bool as = false) noexcept {
        // Make sure there is a target filename
        if (as || filename.empty()) {
            nfdchar_t *selectedFile = nullptr;
            if (NFD_SaveDialog("xml", "", &selectedFile, nullptr) == NFD_OKAY) {
                filename = selectedFile;
                filenameAutoAppendExtension();
                NFD_FreePath(selectedFile);
            } else {
                return false;
            }
        }
        // Save it there
        auto scene = GetScene();
        File ser(GetContext(), filename, FileMode::FILE_WRITE);
        if (lastLookingAt) {
            unhighlight(lastLookingAt);
        }
        sceneDeprep(scene);
        scene->SaveXML(ser);
        scenePrep(scene);
        return true;
    }

    void addResourceDir() noexcept {
        nfdchar_t *selectedFolder = nullptr;
        if (NFD_PickFolder("", &selectedFolder) == NFD_OKAY) {
            auto* cache = context_->GetSubsystem<ResourceCache>();
            cache->AddResourceDir(selectedFolder);
            NFD_FreePath(selectedFolder);
        }
    }

    void filenameAutoAppendExtension() {
        if (!filename.ends_with(".xml")) {
            filename.append(".xml");
        }
    }

    void makeWall(Vector2 from, Vector2 to, bool preview = false) {
        // Round values
        from.x_ = roundMultiple(from.x_, cb->scale);
        from.y_ = roundMultiple(from.y_, cb->scale);
        to.x_ = roundMultiple(to.x_, cb->scale);
        to.y_ = roundMultiple(to.y_, cb->scale);
        // Make wall
        auto wall = GetScene()->GetComponent<Factory>()->makeWall({from, to, cb->floor, wallThickness});
        // Handle preview
        if (preview) {
            wall.root->AddTag("Preview");
            wallPreview = wall;
        }
    }
    void makeFloor(Vector2 from, Vector2 to, bool preview = false) {
        // Round values
        from.x_ = roundMultiple(from.x_, cb->scale);
        from.y_ = roundMultiple(from.y_, cb->scale);
        to.x_ = roundMultiple(to.x_, cb->scale);
        to.y_ = roundMultiple(to.y_, cb->scale);
        // Make floor
        auto floor = GetScene()->GetComponent<Factory>()->makeFloor({from, to, cb->floor});
        // Handle preview
        if (preview) {
            floor.root->AddTag("Preview");
            floorPreview = floor;
        }
    }
    void makeObject(Vector2 from, Vector2 to, bool preview = false) {
        switch (buildMode) {
        case BuildMode::wall: return makeWall(from, to, preview);
        case BuildMode::floor: return makeFloor(from, to, preview);
        }
    }

    void startPreview() {
        makeObject(from, from, true);
    }
    void updatePreview() {
        if (inBuild) {
            if (wallPreview) {
                wallPreview.remove();
            }
            if (floorPreview) {
                floorPreview.remove();
            }
            auto to3D = lastLookingAt->GetWorldPosition();
            to = Vector2(to3D.x_, to3D.z_);
            makeObject(from, to, true);
        }
    }
    void stopPreview() {
        if (wallPreview) {
            wallPreview.remove();
        }
        if (floorPreview) {
            floorPreview.remove();
        }
    }

    static constexpr float highlightY = 0.25f;
    void highlight(Node *node) {
        auto nPos = node->GetWorldPosition();
        if (node->HasTag("CheckerboardTile")) {
            nPos.y_ += highlightY;
        } else {
            nPos.y_ -= highlightY;
        }
        node->SetWorldPosition(nPos);
    }
    void unhighlight(Node *node) {
        auto nPos = node->GetWorldPosition();
        if (node->HasTag("CheckerboardTile")) {
            nPos.y_ -= highlightY;
        } else {
            nPos.y_ += highlightY;
        }
        node->SetWorldPosition(nPos);
    }
};


class App final : public Application {
    Scene *scene;
    Node *walls;
    Camera *camera;

public:
    using Application::Application;

    void Setup() override {
        engineParameters_[EP_VSYNC] = true;
        engineParameters_[EP_FULL_SCREEN] = false;
        engineParameters_[EP_WINDOW_RESIZABLE] = true;
        engineParameters_[EP_WINDOW_MAXIMIZE] = true;
        engineParameters_[EP_WINDOW_TITLE] = "RBFX Wall building tool";
        context_->RegisterFactory<Factory>();
        context_->RegisterFactory<MainController>();
        context_->RegisterFactory<Checkerboard>();
    }

    void Start() override {
        // Create scene
        scene = new Scene(context_);
        scene->CreateComponent<Octree>();
        scene->CreateComponent<PhysicsWorld>();

        scenePrep(scene);
    }
};


void scenePrep(Scene *scene) {
    scene->GetOrCreateComponent<Factory>();
    scene->GetOrCreateComponent<Checkerboard>();
    auto camNode = scene->FindChild("Camera");
    if (!camNode) {
        camNode = scene->CreateChild("Camera");
    }
    auto camLight = camNode->GetOrCreateComponent<Light>();
    camLight->SetLightType(LightType::LIGHT_DIRECTIONAL);
    camLight->SetRange(5.0f);
    camNode->GetOrCreateComponent<MainController>();
}

void sceneDeprep(Scene *scene) {
    scene->RemoveComponent<Factory>();
    scene->GetComponent<Checkerboard>()->destroy();
    scene->RemoveComponent<Checkerboard>();
    auto camNode = scene->FindChild("Camera");
    camNode->RemoveComponent<Camera>();
    camNode->RemoveComponent<MainController>();
    camNode->RemoveComponent<Light>();
}


URHO3D_DEFINE_APPLICATION_MAIN(App)
